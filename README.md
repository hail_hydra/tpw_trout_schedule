# tpw_trout_schedule

## Description

During winter Texas Parks and Wildlife stocks local fishing holes with trout. The TPW [website](https://tpwd.texas.gov/fishboat/fish/management/stocking/trout_stocking.phtml) has a table with the stocking details. However, the table is static and not user-friendly. This codebase creates a Shiny app that:

- imports the static table and makes it interactive
- using a HERE API key can calculate the drive time and distance from a location to the trout-stocked fishing holes

The Shiny app can be accessed [here](https://kaladin-stormblessed.shinyapps.io/Texas_Parks_Wildlife_Trout_Schedule/).

Happy fishing!

## Roadmap

The following is a list of ideas left to implement:

- a way to see how long the drive time calculation will take

## Collaboration

Please feel free to submit a pull request. Collaboration is appreciated and welcomed.

